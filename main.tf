provider "google" {
 version = "3.5.0"
 credentials = file("../compute-instance.json")
 project = "methodical-mark-352721"
 region = "us-west4-b"
 zone = "us-west4-b"
}

resource "google_compute_network" "vpc_network" {
 name = "terraform-network"
}
resource "google_compute_instance" "vm_instance" {
 name = "applicationinstance1"
 machine_type = "n1-standard-1"
 zone = "us-west4-b"
 boot_disk {
  initialize_params {
   image = "debian-cloud/debian-10"
  }
}

network_interface {
 network = google_compute_network.vpc_network.name
 access_config { }
 }
}
