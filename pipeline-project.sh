#!/bin/sh

case "$1" in
	start)
		echo "Starting Pipeline-Project"
		#source env/bin/activate
		flask run --host=0.0.0.0 --port=8080 1>> flaskrun.log &
		echo "Pipeline-Project  started"
		;;
	stop)
		echo "Stopping Pipeline-Project"
		sudo pkill -f python3
		echo "Pipeline-Project stopped"
		;;
	*)
		echo "Usage: ./pipeline-project.sh {start|stop}"
		exit 1
		;;
esac

exit 0
